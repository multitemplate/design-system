# MTL - MultiTempLate

## Quick start

1. Check do you have Node.ja and Yarn
2. Clone repository  `git clone git@bitbucket.org:multitemplate/html.git`
3. Navigate to cloned directory
4. Install node modules `yarn install`
5. Run application `yarn start`
6. Application automatically open in browser.

## Directory structure 

* dist - build dir
* src

    * assets - images, global styles, scripts
    * components - template files with styles
    * layouts - global project layouts 
    * pages
    * views - section structures with styles
    

## Add new views or components

1. Create new dir, ex: `Paragraph`
2. Create new files. `Paragraph.pug` and `Paragraph.scss`
3. Import styles in `app.scss`
4. Include pug file in `Index.pug` or in other file

## used libraries 
* https://getbootstrap.com/docs/5.0/layout/breakpoints/
* https://github.com/material-components/material-components-web