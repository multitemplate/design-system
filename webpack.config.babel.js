import webpack 						from 'webpack';
import autoprefixer 				from 'autoprefixer';
import path 						from 'path';
import WebpackNotifierPlugin 		from 'webpack-notifier';
import MiniCssExtractPlugin 		from 'mini-css-extract-plugin';
import CssMinimizerPlugin 			from 'css-minimizer-webpack-plugin';
import TerserPlugin 				from 'terser-webpack-plugin';
import HtmlWebpackPlugin 			from 'html-webpack-plugin';
import ESLintPlugin 				from 'eslint-webpack-plugin';
import CleanTerminalPlugin 			from 'clean-terminal-webpack-plugin';
const appVersion 					= require('./package.json').version;

module.exports = (_env, argv) => {
	return {
		context: path.resolve(__dirname, 'src'),
		entry: {
			example: [ './example.js', './example.scss' ],
		},
		output: {
			path: path.resolve(__dirname, './example/'),
			publicPath: '/',
		},
		module: {
			rules: [
				{
					test: /\.pug$/i,
					loader: 'pug-loader',
					options: { preaty: true },
				},
				{
					test: /\.(js)$/,
					exclude: /node_modules/,
					use: [ 'babel-loader' ],
				},
				{
					test: /\.(scss|css)$/,
					exclude: [ /webfonts/ ],
					use: [
						argv.mode === 'production' ? MiniCssExtractPlugin.loader : 'style-loader',
						{ loader: 'css-loader', options: { sourceMap: true } },
						{ loader: 'postcss-loader', options: { sourceMap: true } },
						{ loader: 'sass-loader', options: {
							sourceMap: true,
							implementation: require('sass'),
						} },
					],
				},
			],
		},
		plugins: [
			new CleanTerminalPlugin({
				message: 'Console clearing...',
				onlyInWatchMode: true,
				skipFirstRun: true,
				beforeCompile: true,
			}),
			new ESLintPlugin(),
			new WebpackNotifierPlugin({
				skipFirstNotification: true,
				emoji: true,
			}),
			new MiniCssExtractPlugin({
				filename: `[name].css?v=${appVersion}`,
			}),
			new webpack.LoaderOptionsPlugin({
				options: {
					postcss: [
						autoprefixer(),
					],
				},
			}),
			new webpack.optimize.LimitChunkCountPlugin({
				maxChunks: 1,
			}),
			new HtmlWebpackPlugin({
				version: appVersion,
				DEV: argv.mode !== 'production',
				filename: 'index.html',
				template: './example.pug',
				path: path.resolve(__dirname, './example'),
				inject: 'body',
			}),
			new webpack.DefinePlugin({
				DEV: argv.mode !== 'production',
			}),
		],
		resolve: {
			modules: [
				'node_modules',
			],
			extensions: [ '.js', '.pug' ],
			alias: {
				utils: path.resolve(__dirname, 'src/scss/utils/'),
			},
		},
		devtool: 'source-map',
		externals: {
			'jquery': 'jQuery',
		},
		optimization: {
			runtimeChunk: 'single',
			minimize: argv.mode === 'production',
			minimizer: [
				new CssMinimizerPlugin(),
				new TerserPlugin({
					test: /\.js(\?.*)?$/i,
				}),
			],
		},
		target: 'web',
		devServer: {
			index: 'index.html',
			host: '0.0.0.0',
			useLocalIp: true,
			port: 3000,
			hot: true,
			open: true,
			watchContentBase: true,
			watchOptions: {
				aggregateTimeout: 1500,
			},
			overlay: {
				warnings: false,
				errors: true,
			},
		},
	};
};
