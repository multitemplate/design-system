import $ from 'jquery';

(() => {
	$('input[type="checkbox"], input[type="radio"]').on('change', (event) => {
		if (event.target.parentElement.className.includes('__wrapper')) {
			if (!event.target.checkValidity()) {
				$(event.target.parentElement).addClass('invalid');
				if ($(event.target.parentElement).has('.input__warningInfo').length === 0) {
					$(event.target.parentElement).append('<div class="input__warningInfo"></div>');
				}
				$(event.target).siblings('.input__warningInfo')[0].innerText = event?.target?.validationMessage;
			} else {
				$(event.target.parentElement).removeClass('invalid');
				if ($(event.target.parentElement).has('.input__warningInfo').length !== 0) {
					$(event.target).siblings('.input__warningInfo')[0].innerText = '';
				}
			}
		} else {
			if (DEV) {
				console.warn('ParentElement with class includes "__wrapper" not exist for element: ', event.target);
			}
		}
	});

	// Basic input walidation based on HTML5 validity
	$('input').on('blur', (event) => {
		if (event.target.parentElement.className.includes('__wrapper')) {
			if (!event.target.checkValidity()) {
				$(event.target.parentElement).addClass('invalid');
				if ($(event.target.parentElement).has('.input__warningInfo').length === 0) {
					$(event.target.parentElement).append('<div class="input__warningInfo"></div>');
				}
				$(event.target).siblings('.input__warningInfo')[0].innerText = event?.target?.validationMessage;
			} else {
				$(event.target.parentElement).removeClass('invalid');
				$(event.target).siblings('.input__warningInfo')[0].innerText = '';
			}
		} else {
			if (DEV) {
				console.warn('ParentElement with class includes "__wrapper" not exist for element: ', event.target);
			}
		}
	});
	$('input:disabled').each((_index, element) => {
		if (element.parentElement.className.includes('__wrapper')) {
			$(element.parentElement).addClass('disabled');
		} else {
			if (DEV) {
				console.warn('ParentElement with class includes "__wrapper" not exist for element: ', element);
			}
		}
	});
})();
