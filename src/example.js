import $ from 'jquery';
import './js/index';

$(() => {
	$('#tocListFinder section').each((_index, element) => {
		$('#tocContent').append(`<li> <a href='#${element?.id}'>${element?.id}</a></li>`);
	});
});
